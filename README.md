# Open Current Browser Tab WITH..

A simple Windows Autohotkey script that adds keyboard shortcuts for executing various actions on your active browser tab's url. 

- Open with different browser (Chrome, Firefox, Brave, etc)
- Open on different device through KDE-Connect
- Save to Pocket
- Send to a Telegram channel using a bot 
  - Handy for saving for later, or to to open up on a different device
- Quick copy URL to clipboard 
- Open last archived version through Archive.org
- Download Video or Audio using yt-dlp

![Preview](https://gitlab.com/yman84/open-with-browser/uploads/6be99a4dca865cefbc008136d68846c8/Preview.gif)

## Default Keyboard Shortcuts

##### (Can be customized by editing the .ahk file)

- Alt + M: Send to Telegram Channel

- Alt + P: Save Page to Pocket

- Alt + X: Copy URL to Clipboard

- Open Tab in Another Browser
  
  - Alt + F: Firefox
  
  - Alt + Shift + F: Firefox Private
  
  - Alt + C: Chrome
  
  - Alt + Shift + C: Chrome Incognito
  
  - Alt + B: Brave
  
  - Alt + Shift + B: Brave Incognito

- Open Archived Version Through Archive.org
  
  - Alt + A: Open in same Same Tab
  
  - Alt + Shift + A: Open Page in New Tab

- Open on Different Device using KDE-Connect
  
  - Alt + O: Open with KDE-Connect Device One
  
  - Alt + Shift + O: Open with KDE-Connect Device Two

- Download Video using yt-dlp
  
  - Alt + Y: Download Video
  
  - Alt + Shift + Y: Download Audio

# Installation

1) Make sure you have [autohotkey](https://www.autohotkey.com/) installed

2) If you have git installed:

        `git clone https://gitlab.com/yman84/open-with-browser.git`

      Otherwise:

        Click `Download` and then select `zip` 

# Extended Functionality Configuration

## Open Tab on Different Device(s) using KDE-Connect

- Install KDE-Connect: https://kdeconnect.kde.org/download.html
- Get device IDs by running the following in Comamnd Prompt: 
    ``"C:\Program Files\KDE Connect\bin\kdeconnect-cli.exe" -l``
- Set the KDEConnectDeviceOne\Two\Three in `KDE Connect Settings` section of the .ahk script to the device ID of each device you want to send to

## Save Tab URL to Pocket

- Obtain an Access Token and Consumer Key by following the [Pocket docs](https://getpocket.com/developer/docs/authentication):
- Input both the Token and Key into the `Pocket Settings` section of the .ahk script

## Send Tab URL to a Telegram Channel

- Create a Telegram Bot by following the [Telegram docs](https://core.telegram.org/bots#6-botfather)
- Obtain your chatID by following the guide [here](https://stackoverflow.com/questions/32423837/telegram-bot-how-to-get-a-group-chat-id)
- Input both numbers into the `Telegram Settings` section of the .ahk file

## Download Video with [yt-dlp](https://github.com/yt-dlp/yt-dlp)

- Install yt-dlp 
  
  - Using Chocolatey: ``choco install yt-dlp``
  
  - OR [Manually](https://github.com/yt-dlp/yt-dlp/releases/latest)

- Update the `yt-dlp Settings` section of the .ahk file to match your video/audio quality preferences
  
  - By default video is 720p and audio is "BEST"

## Add Another Browser to Open Tabs With

- Add your browser's filepath to the `Filepaths of Browsers` section below the others
- Add your browser's ahk_exe info below the other browsers in `Trigger Within Applications` section by using Window Spy.ahk (comes installed with Autohotkey)
- Change the function call to reflect your new browser
  - eg: `OpenWithBrowser(Firefox)` >> `OpenWithBrowser(YourCustomBrowser)`